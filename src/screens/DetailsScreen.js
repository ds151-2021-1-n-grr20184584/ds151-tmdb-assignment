import React, { useEffect, useState } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import tmdb from '../api/tmdb';

const DetailsScreen = ({ navigation, route }) => {
  const [details, setDetails] = useState({});

  const id = route.params.id;
  const type = route.params.type;
  const imageBaseUrl = 'https://image.tmdb.org/t/p/w400';

  useEffect(async () => {
    const response = await tmdb.get(`/${type}/${id}`);

    setDetails(() => response.data);
    console.log(response.data);
  }, []);

  // tipo de componente dinâmico, simulação de switch expression
  const Detail = () => {
    const Table = ({ children }) => (
      <View style={styles.table}>{children}</View>
    );
    const Row = ({ children }) => <View style={styles.row}>{children}</View>;
    const Name = ({ children }) => <Text style={styles.name}>{children}</Text>;
    const Value = ({ children }) => (
      <Text style={styles.value}>{children}</Text>
    );

    return {
      movie() {
        const releaseDate = details.release_date.split('-');
        const releaseDateString = `${releaseDate[2]}/${releaseDate[1]}/${releaseDate[0]}`;

        return (
          <Table>
            <Row>
              <Name>Title</Name>
              <Value>{details.original_title}</Value>
            </Row>
            <Row>
              <Name>Released in</Name>
              <Value>{releaseDateString}</Value>
            </Row>
            <Row>
              <Name>Produced by</Name>
              <Value>
                {details.production_companies.length < 1
                  ? ' - '
                  : details.production_companies.map((c) => c.name).join(', ')}
              </Value>
            </Row>
            <Row>
              <Name>Rating</Name>
              <Value>{details.vote_average}</Value>
            </Row>
          </Table>
        );
      },
      person() {
        const birthday = details.birthday.split('-');
        const birthdayString = `${birthday[2]}/${birthday[1]}/${birthday[0]}`;

        return (
          <Table>
            <Row>
              <Name>Name</Name>
              <Value>{details.name}</Value>
            </Row>
            <Row>
              <Name>Birthday</Name>
              <Value>{birthdayString}</Value>
            </Row>
            <Row>
              <Name>Place of birth</Name>
              <Value>{details.place_of_birth}</Value>
            </Row>
          </Table>
        );
      },
      tv() {
        const releaseDate = details.first_air_date.split('-');
        const releaseDateString = `${releaseDate[2]}/${releaseDate[1]}/${releaseDate[0]}`;

        return (
          <Table>
            <Row>
              <Name>Title</Name>
              <Value>{details.original_name}</Value>
            </Row>
            <Row>
              <Name>Released in</Name>
              <Value>{releaseDateString}</Value>
            </Row>
            <Row>
              <Name>Produced by</Name>
              <Value>
                {details.production_companies.length < 1
                  ? ' - '
                  : details.production_companies.map((c) => c.name).join(', ')}
              </Value>
            </Row>
            <Row>
              <Name>No. of Episodes</Name>
              <Value>{details.number_of_episodes}</Value>
            </Row>
            <Row>
              <Name>No. of Seasons</Name>
              <Value>{details.number_of_seasons}</Value>
            </Row>
            <Row>
              <Name>Rating</Name>
              <Value>{details.vote_average}</Value>
            </Row>
          </Table>
        );
      },
    }[type]();
  };

  return (
    <View>
      <Image
        style={styles.image}
        source={{ uri: `${imageBaseUrl}${details.poster_path}` }}
      />
      {details.original_title || details.name || details.original_name ? (
        <Detail />
      ) : (
        <></>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
  image: {
    width: 300,
    height: 350,
    alignSelf: 'center',
  },
  table: {
    margin: 20,
    borderWidth: 1,
    borderColor: '#555',
  },
  row: {
    flexDirection: 'row',
    borderColor: '#ccc',
    borderWidth: 1,
  },
  name: {
    fontWeight: 'bold',
    flex: 2,
    backgroundColor: '#ddd',
    fontSize: 16,
  },
  value: {
    fontSize: 16,
    flex: 3,
  },
});

export default DetailsScreen;
