import React, { useEffect, useState } from 'react';
import {
  TouchableOpacity,
  FlatList,
  Text,
  View,
  StyleSheet,
} from 'react-native';
import SearchBar from '../components/SearchBar';
import tmdb from '../api/tmdb';
import Item from '../components/Item';

const HomeScreen = ({ navigation }) => {
  const [data, setData] = useState([]);
  const [type, setType] = useState('movie');
  const [lastSearch, setLastSearch] = useState('');

  const handleSearch = async (query) => {
    const response = await tmdb.get(`/search/${type}`, {
      params: {
        query,
      },
    });

    setData(() => response.data.results);
    setLastSearch(() => query);
  };

  useEffect(async () => {
    if (lastSearch != '') await handleSearch(lastSearch);
  }, [type]);

  return (
    <View>
      <SearchBar onSearch={handleSearch} />
      <View style={styles.types}>
        <TouchableOpacity
          style={
            type == 'movie'
              ? { ...styles.type, ...styles.activeType }
              : styles.type
          }
          onPress={() => setType('movie')}
        >
          <Text
            style={
              type == 'movie'
                ? { ...styles.text, ...styles.activeText }
                : styles.text
            }
          >
            Movie
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={
            type == 'person'
              ? { ...styles.type, ...styles.activeType }
              : styles.type
          }
          onPress={() => setType('person')}
        >
          <Text
            style={
              type == 'person'
                ? { ...styles.text, ...styles.activeText }
                : styles.text
            }
          >
            Person
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={
            type == 'tv'
              ? { ...styles.type, ...styles.activeType }
              : styles.type
          }
          onPress={() => setType('tv')}
        >
          <Text
            style={
              type == 'tv'
                ? { ...styles.text, ...styles.activeText }
                : styles.text
            }
          >
            TV
          </Text>
        </TouchableOpacity>
      </View>
      <FlatList
        data={data}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => (
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('Details', { id: item.id, type })
            }
          >
            <Item
              id={item.id}
              image={item.poster_path || item.profile_path}
              title={item.original_title || item.name || item.original_name}
            />
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  types: {
    flexDirection: 'row',
  },
  type: {
    flex: 1,
    borderWidth: 1,
    borderRadius: 20,
    padding: 10,
    margin: 5,
  },
  activeType: {
    backgroundColor: '#000',
  },
  text: {
    textAlign: 'center',
    fontSize: 16,
    color: '#000',
  },
  activeText: {
    color: '#fff',
  },
});

export default HomeScreen;
