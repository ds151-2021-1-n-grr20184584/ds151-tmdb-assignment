import React, { useState } from 'react';
import { FontAwesome } from '@expo/vector-icons';
import { View, TextInput, StyleSheet } from 'react-native';

const SearchBar = ({ onSearch }) => {
  const [text, setText] = useState('');

  return (
    <View style={styles.container}>
      <View style={styles.searchBox}>
        <FontAwesome
          style={styles.icon}
          name="search"
          size={24}
          color="black"
        />
        <TextInput
          style={styles.inputBox}
          value={text}
          onChangeText={(t) => setText(() => t)}
          onEndEditing={() => onSearch(text)}
        ></TextInput>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
  searchBox: {
    padding: 5,
    flexDirection: 'row',
    borderRadius: 10,
    backgroundColor: 'lightgray',
  },
  inputBox: {
    padding: 5,
    flex: 1,
  },
  icon: {
    margin: 4,
  },
});

export default SearchBar;
