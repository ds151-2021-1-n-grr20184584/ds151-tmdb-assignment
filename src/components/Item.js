import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';

const Item = ({ title, id, image }) => {
  const imagePath = `https://image.tmdb.org/t/p/w200/${image}`;
  return (
    <View style={styles.container}>
      <Image style={styles.image} source={{ uri: imagePath }} />
      <View style={styles.text}>
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.title}>{title}</Text>
        </View>
        <Text>#{id}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 10,
  },
  image: {
    marginRight: 15,
    width: 100,
    height: 100,
    borderRadius: 6,
  },
  text: {
    flex: 1,
  },
  title: {
    fontSize: 18,
    flexWrap: 'wrap',
    marginBottom: 5,
  },
});

export default Item;
