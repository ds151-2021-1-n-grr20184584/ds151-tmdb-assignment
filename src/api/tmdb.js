import { API_KEY } from '@env';
import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.themoviedb.org/3/',
  params: {
    api_key: API_KEY,
  },
});
